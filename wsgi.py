from flask import Flask, request
from multiprocessing import Lock
import os
import time

mutex = Lock()
logdir = 'logs/'
application = Flask(__name__)

if not os.path.exists(logdir):
    try:
        os.makedirs(logdir)
    except OSError as error:
        if error.errno != errno.EEXIST:
            raise

@application.route('/', methods = ['GET', 'POST'])
def getData():
    if request.method == 'GET':
        nodename = request.args.get('name', type = str)
        if not nodename:
            return 'Please specify name of the node'
        logfile = logdir + nodename
        with mutex:
            if not os.path.isfile(logfile):
                return 'No data received yet for this node'
            with open(logfile, 'r') as f:
                return f.read()
    elif request.headers['Content-Type'] == 'text/plain':
        reqdata = request.data.split(':')
        logfile = logdir + reqdata[0]
        with mutex:
            with open(logfile, 'ab+') as f:
                f.write(time.strftime('%Y.%m.%d, %H:%M:%S >> ' ) + reqdata[1] + '\n')
            return 'Received data: ' + request.data

if __name__ == '__main__':
    with mutex:
        if not os.path.exists(logdir):
            os.makedirs(logdir)
    app.run()
